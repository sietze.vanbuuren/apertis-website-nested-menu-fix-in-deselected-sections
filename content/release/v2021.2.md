+++
weight = 100
title = "v2021.2"
+++

# Release v2021.2

- {{< page-title-ref "/release/v2021.2/release_schedule.md" >}}
- {{< page-title-ref "/release/v2021.2/releasenotes.md" >}}
