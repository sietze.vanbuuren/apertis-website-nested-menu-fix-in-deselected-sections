+++
title = "The Apertis Platform"
weight = 100
toc = true

aliases = [
    "/old-developer/latest/platform-guide.html",
    "/old-developer/v2019/platform-guide.html",
    "/old-developer/v2020/platform-guide.html",
    "/old-developer/v2021pre/platform-guide.html",
    "/old-developer/v2022dev0/platform-guide.html",
    "/old-wiki/Apertis:About"
]

date = "2020-11-11"
lastmod = "2021-04-29"
+++

Apertis is a versatile open source distribution and associated infrastructure,
originally tailored to automotive use cases, but which has grown to be a fit
for a wide variety of electronic devices. While Apertis provides components
both for deployment on target devices and to meet the needs of development
workflows, these are clearly separated, with special care taken to ensure that
components intended for deliverables are free from
[problematic licensing constraints]({{< ref "license-expectations.md" >}}).

Apertis is not just a Debian-derived GNU/Linux distribution. It comprises: code
hosting; code review tools; package build and image generation services; and an
automated testing infrastructure with the aim of providing a clean, reliable
environment. This allows developers to go from source to deployable system
images in the most dependable way, ready to be hosted on the cloud, programmed
into a custom device or made available via
[over-the-air (OTA) updates]({{< ref "ostree.md" >}}).

![](/images/apertis-functional-view.svg)

While the typical embedded workflow only supports one product line at a time,
Apertis focuses on a collaborative development, aiming to be the integration
point for multiple different independent product lines, each with different
goals and different schedules, but all able to share the same common core.

Apertis provides the tools to needed to maximize the commonalities that can be
shared across different lines of development, whilst also providing the ability
for each product to experiment and differentiate without impacting the shared
core.

# Collaborative development

Apertis is designed for a collaborative development model with the aim of
sharing efforts across and between multiple independent participants. It offers
[workflows]({{< ref "workflow-guide.md" >}}) and tools to maximize the shared
commonalities, this reduces costs and increases development speed. This
contrasts with approaches where participants of each development team are
independently working towards there own goals, duplicating effort to implement
common features.

While other workflows only focus on one team at a time, Apertis' strength is
its ability to support many independent teams concurrently. The development
infrastructure of Apertis runs as a service to provide shared access to all
those independent product teams:

- **By connecting the software packages and the infrastructure through a
  well-defined interface, their mutual independence is guaranteed**: This means
  that there should be no case where including an updated package requires
  changes to the infrastructure and, vice-versa, infrastructural changes do not
  require existing packages to be updated, reducing the maintenance burden over
  the long term
- **Common infrastructure is provided to developers**: As a result developers
  do not need to go through the long and subtly error-prone process of setting
  up their own full build environment.
- **Tests get run on supported hardware devices automatically**: By having
  existing test infrastructure, development teams are saved the burden of
  setting up their own test lab from scratch, enabling them to be running tests
  automatically  on their supported hardware devices and device variants
  quickly.
- **What is tested is what ends up deployed on production devices**: Apertis
  is able to run component unit tests as part of the centralized compilation
  and packaging process with additional system testing performed on
  automatically generated product images.

On top of the shared core, each user has access to
[dedicated project areas](contributions.md#dedicated-project-areas) for
components that are not meant to be shared with other teams, both for
experimentation and for product development.

Contributions to the shared Apertis components follow Open Source Software
(OSS) best practices with its
[maintainer/contributor policies]( {{< ref "contributions.md" >}} )
centered on code review and
[continuous integration]( {{< ref "component_structure.md" >}} ).

# Security driven

The Apertis approach is driven by the need to increase safety and security by
deploying updated software in the hands of users in a timely and efficient way.

Apertis is primarily
[built upon the latest Debian Stable]({{< ref "case-for-moving-to-debian.md" >}})
sources enabling Apertis to directly benefit from its long term quality
management, steady flow of fixes, compatibility and maturity, as shared by
Debian and by its many derived distributions. By closely tracking Debian and a
few other key upstream components, Apertis benefits from their well-defined
[CVE](http://cve.mitre.org) processes to identify urgent issues that affect
packages hosted in their repositories and quickly act on those. Urgent patches
like those fixing exploitable CVEs are merged timely into Apertis and are
immediately available for downstream products.  A quarterly release cycle
provides a way for product teams to get access to a
[stable stream]({{< ref "release-flow.md#apertis-release-flow" >}}) of less
urgent updates.

In order to protect Apertis systems from the exploitation of both known and
unknown application flaws, by either external or internal threats, Apertis
utilizes [AppArmor]({{< ref "guides/apparmor.md" >}}) for policy enforcement. This
enables the system to restrict each applications access to only those resources
that they are meant to be using, providing mitigation against many potential
attack vectors.

The optional [OSTree-based update mechanism]({{< ref "ostree.md" >}}) provides
an efficient and safe update facility for the base platform, such that updates
can be deployed often with minimal costs. The
[guide]({{< ref "deployment-management.md" >}}) and demonstration
installation of [hawkBit](https://www.eclipse.org/hawkbit/) providing an
example of how to implement fleet manangement.

![hawkBit using OSTree static deltas for OTA update](/images/ostree-hawkbit-static-deltas.svg)

Applications can be deployed and updated in
[application bundles]({{< ref "canterbury-legacy-application-framework.md" >}})
(with a new [Flatpak-based solution]({{< ref "application-framework.md" >}}) on
the way) without re-deploying the whole platform, decoupling the release and
update cycle of the base software from that of each application.

A goal of Apertis is to reduce the efforts required for long-term maintenance,
in particular to enable quick and consistent response times for security issues
in internet-enabled products.  The package-centric solution and shared
infrastructure offered by Apertis enable all the involved teams to maximize
commonalities across products to ensure the safety of deployed products in a
timely manner as part of an economically sustainable long term strategy.

# Package-centric approach

With Apertis, developers can focus on developing the components that provide
their unique experience and rely on the shared core components and infrastructure
for everything else.

Apertis builds on the proven practice, used by Debian and many other major
distributions, separating the compilation of individual components into binary
packages from the composition of images destined for target devices. This
package-centric approach is the key enabler at the center of all activities,
tools and processes in Apertis. The selection and selective customization of
these binary packages enables deployable images to be created quickly and
efficiently for a variety of different products and product variants via
automated processes.

![Basic workflow from source to image](/images/workflow_basic-source-to-image.svg)

The Apertis infrastructure enables the resources for processes like compilation
to be shared across all users, with changes to common components getting
processed once and resulting binaries being shared with all. These resources
are available to every team and since they are provided as a service they do
not need to be duplicated for each developer. This ensures reproducibility,
traceability and consistency during the whole product life cycle.

# Key technologies

Apertis makes heavy use of many open source technologies to form a strong platform:

- **Debian packages** provide a wide ecosystem of pre-packaged components, from
  a project with a long history of providing a reliable, robust operating
  system.
- **systemd** for system and service management, as well providing many basic
  services.
- **AppArmor** adding Mandatory Access Control, providing policy enforcement of
  application profiles.
- **OSTree** and **Flatpak** for safe, efficient and modular deployments
- **D-Bus** providing inter-process communications, enabling privilege separation between the various system services and applications.
- **Wayland**-based compositor, providing a modern efficient display server.
- **GStreamer** enabling multimedia playback
