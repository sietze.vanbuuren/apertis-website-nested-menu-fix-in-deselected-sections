+++
date = "2018-06-25"
weight = 100

title = "abstract-contact-addressbook-creation"

aliases = [
    "/old-wiki/QA/Test_Cases/abstract-contact-addressbook-creation"
]
+++
This test case has now been made obsolete. Current test definitions are now available at https://qa.apertis.org/
