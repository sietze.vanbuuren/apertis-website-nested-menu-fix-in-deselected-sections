+++
date = "2018-07-12"
weight = 100

title = "boot-performance"

aliases = [
    "/old-wiki/QA/Test_Cases/boot-performance"
]
+++
This test case has now been made obsolete. Current test definitions are now available at https://qa.apertis.org/
