+++
date = "2018-06-25"
weight = 100

title = "sdk-code-analysis-tools-splint-smoke-test"

aliases = [
    "/old-wiki/QA/Test_Cases/sdk-code-analysis-tools-splint-smoke-test"
]
+++
This test case has now been made obsolete. Current test definitions are now available at https://qa.apertis.org/
