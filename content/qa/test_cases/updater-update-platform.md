+++
date = "2017-11-22"
weight = 100

title = "updater-update-platform"

aliases = [
    "/old-wiki/QA/Test_Cases/updater-update-platform"
]
+++
This test case has now been made obsolete. Current test definitions are now available at https://qa.apertis.org/
