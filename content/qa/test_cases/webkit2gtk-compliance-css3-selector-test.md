+++
date = "2017-03-30"
weight = 100

title = "webkit2gtk-compliance-css3-selector-test"

aliases = [
    "/old-wiki/QA/Test_Cases/webkit2GTK-compliance-css3-selector-test"
]
+++
This test case has now been made obsolete. Current test definitions are now available at https://qa.apertis.org/
