+++
date = "2018-06-25"
weight = 100

title = "sdk-performance-tools-gprof-smoke-test"

aliases = [
    "/qa/test_cases/sdk-performance-tools-gprof.md",
    "/old-wiki/QA/Test_Cases/sdk-performance-tools-gprof",
    "/old-wiki/QA/Test_Cases/sdk-performance-tools-gprof-smoke-test"
]
+++
This test case has now been made obsolete. Current test definitions are now available at https://qa.apertis.org/
