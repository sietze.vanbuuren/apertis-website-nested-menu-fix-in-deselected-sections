+++
date = "2019-10-25"
weight = 100

title = "Package maintenance"

aliases = [
    "/old-wiki/Package_maintenance"
]
+++

Apertis hosts its own package subset on the
[Apertis GitLab instance](https://gitlab.apertis.org/). On successful
completion of the CI pipeline these are uploaded to
[Collabora's Open Build Service (OBS) instance](https://build.collabora.co.uk)
where the packages are formally built and hosted for Apertis.

## Packages Guidelines

The package set is distributed in several groups: `target`,
`development`, `sdk`, `hmi`, `helper-libs`. Those groups are merged in
a single repository, and split in what distribution names repository
components (`target`, `development`, `sdk`, `hmi`, `helper-libs`).

Each of these repository components have different constraints regarding
the type of package that can be hosted. Check the
[licensing]( {{< ref "license-applying.md" >}} )
documentation for details.

  - Source packages modified from upstream distribution include a
    versioning suffix 'co'.
  - Source packages in SDK are built only for supported SDK
    architectures (Intel 64 bit)
  - Source packages in `target`, `development`, `hmi`, `helper-libs` are
    built for all supported architectures (ARM hard float, ARM 64bit,
    Intel 64 bit)

Target images contain `target`, `hmi` components. At build time, target
images can refer to `development` components.

## Requesting a new package

If a new package is desired, the request should be made following the relevant 
[process]( {{< ref "contributions.md#extending-apertis" >}} ).
