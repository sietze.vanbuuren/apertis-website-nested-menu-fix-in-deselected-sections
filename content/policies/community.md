+++
date = "2019-11-15"
weight = 100

title = "Community"

aliases = [
    "/old-wiki/Community"
]
+++

Apertis is a free software project which is designed and developed in
the open. The Apertis community is made up of both contributors who work
to improve Apertis and users.

## Join the community

The best ways to get in touch with the community are through:

  - [Mailing lists](https://lists.apertis.org/)
      - The [devel mailing
        list](https://lists.apertis.org/listinfo/devel) is the place to
        discuss Apertis development and project
      - If you are interested in adding a new mailing list, please
        contact *listmaster at apertis.org*

Remember that all contributors to Apertis are expected to [treat others
with respect]( {{< ref "code_of_conduct.md" >}} ).
